<!DOCTYPE html>

<html>

<head>

    <title>Temper Onboarding Flow Percentages</title>

</head>

<body>

<div class="title m-b-md">
    Temper Onboarding Flow Percentages
</div>
<div id="container"></div>

</body>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/datagrouping.js"></script>

<script type="text/javascript">

    var styled_data =  <?php echo $styled_data ?>;
    var main_array = [];
    for(var i = 29; i< 33; i++)
    {
        var arr = [];
        for(x in styled_data[i])
        {
            arr.push(styled_data[i][x]);
        }
        arr.push(100);
        arr.reverse();
        main_array.push(arr);
    }

    function getWeekData(week_nbr)
    {
        return main_array[week_nbr];
    }

    var series1 = {
            name: '29',
            data: getWeekData(0)
        },
        series2 = {
            name: '30',
            data: getWeekData(1)
        },
        series3 = {
            name: '31',
            data: getWeekData(2)
        },
        series4 = {
            name: '32',
            data: getWeekData(3)
        };


    Highcharts.chart('container', {
        title: {
            text: 'Onboarding Flow'
        },
        subtitle: {
            text: 'Source: https://docs.google.com/document/d/14EhAtRp3XVCkIcl68WXPRPlD6pzx0_LDMDBv-GnP70E'
        },
        xAxis: {
            type: 'percentage',
            max: 8,
            categories: ['', 'Approval', 'Waiting for approval','Are you a freelancer?', 'Do you have relevant experience in these jobs?','What jobs are you interested in?','Provide profile information','Activate account','Create account']

        },
        yAxis: {
            max: 100,
            title: {
                text: 'Percentage of users'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        series: [
            series1,
            series2,
            series3,
            series4
        ],

    });

</script>

</html>