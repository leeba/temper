<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Temper</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="container" id="app"></div>
    <div class="content">
        <div class="title m-b-md">
            Temper
{{--            {{ dd($obfData[1]) }}--}}
        </div>

    </div>
</div>

<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
    ]); ?>
</script>

</body>
</html>
