// app.js

require('./bootstrap');

window.Vue = require('vue');

var Highcharts = require('highcharts');
// Load module after Highcharts is loaded
require('highcharts/modules/exporting')(Highcharts);
// Create the chart
window.Vue = Highcharts.chart('container', { /*Highcharts options*/ });

Vue.component('coin-add-component', require('./components/AddComponent.vue'));

const app = new Vue({
    el: '#app'
});