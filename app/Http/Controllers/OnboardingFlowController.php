<?php

namespace App\Http\Controllers;

use App\Adapters\Interfaces\HighchartsAdapterInterface;
use App\Repositories\Interfaces\OnboardingFlowRepositoryInterface;
use Illuminate\Routing\Controller as BaseController;

class OnboardingFlowController extends BaseController
{
    //Repository pattern to facilitate moving to a different data source in future if needed
    private $on_boarding_flow_repository;
    private $highcharts_line_series_adapter;

    public function __construct(OnboardingFlowRepositoryInterface $on_boarding_flow_repository, HighchartsAdapterInterface $highcharts_line_series_adapter)
    {
        $this->on_boarding_flow_repository = $on_boarding_flow_repository;
        $this->highcharts_line_series_adapter = $highcharts_line_series_adapter;
    }

    public function index()
    {
        //Getting raw data
        $obf_data = $this->on_boarding_flow_repository->getAll();

        $styled_data = $this->highcharts_line_series_adapter->prepareDataForVisualization($obf_data);
        $styled_data = json_encode($styled_data);
        return view('chart', compact("styled_data"));
    }

    public function getAllOBFData()
    {
        $obfData = $this->on_boarding_flow_repository->getAll();
        $styled_data = $this->highcharts_line_series_adapter->prepareDataForVisualization($obfData);
        return response()->json($styled_data);
    }

}

