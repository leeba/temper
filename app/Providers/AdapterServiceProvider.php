<?php

namespace App\Providers;

use App\Adapters\HighchartsLineSeriesAdapter;
use App\Adapters\Interfaces\HighchartsAdapterInterface;
use Illuminate\Support\ServiceProvider;

class AdapterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HighchartsAdapterInterface::class,
            HighchartsLineSeriesAdapter::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

}