<?php

namespace App\Providers;

use App\Repositories\Interfaces\OnboardingFlowRepositoryInterface;
use App\Repositories\OnboardingFlowRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(OnboardingFlowRepositoryInterface::class,
            OnboardingFlowRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
