<?php

namespace App\Models\Builders;

use App\Models\OnboardingFlow;
use App\Models\Builders\Interfaces\OnBoardingFlowBuilderInterface;

class OnBoardingFlowBuilder implements OnBoardingFlowBuilderInterface
{
    private $on_boarding_flow;

    public function __construct(OnboardingFlow $on_boarding_flow)
    {
        $this->on_boarding_flow = $on_boarding_flow;
    }

    public function setUserId($user_id)
    {
        $this->on_boarding_flow->user_id = $user_id;
    }

    public function setCreatedAt($created_at)
    {
        $this->on_boarding_flow->created_at = $created_at;
    }

    public function setOnboardingPercentage($onboarding_perentage)
    {
        $this->on_boarding_flow->onboarding_perentage = $onboarding_perentage;
    }

    public function setCountApplications($count_applications)
    {
        $this->on_boarding_flow->count_applications = $count_applications;
    }

    public function setCountAcceptedApplications($count_accepted_applications)
    {
        $this->on_boarding_flow->count_accepted_applications = $count_accepted_applications;
    }

    public function getOnboardingFlow()
    {
        return $this->on_boarding_flow;
    }
}