<?php

namespace App\Models\Builders\Interfaces;

interface OnBoardingFlowBuilderInterface
{
    public function setUserId($user_id);
    public function setCreatedAt($created_at);
    public function setOnboardingPercentage($onboarding_perentage);
    public function setCountApplications($count_applications);
    public function setCountAcceptedApplications($count_accepted_applications);
    public function getOnboardingFlow();
}
