<?php

namespace App\Models\Directors;

use App\Models\Builders\Interfaces\OnBoardingFlowBuilderInterface;

class OnBoardingFlowDirector
{
    public function build(OnBoardingFlowBuilderInterface $builder, $data)
    {
        $builder->setUserId($data[0]);
        $builder->setCreatedAt($data[1]);
        $builder->setOnboardingPercentage($data[2]);
        $builder->setCountApplications($data[3]);
        $builder->setCountAcceptedApplications($data[4]);

        return $builder->getOnboardingFlow();
    }
}