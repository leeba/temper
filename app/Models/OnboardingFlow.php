<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OnboardingFlow extends Model
{
    protected $fillable = [
        'user_id', 'created_at','onboarding_perentage','count_applications','count_accepted_applications'
    ];

}