<?php

namespace App\Adapters\Interfaces;

interface HighchartsAdapterInterface
{
    public function prepareDataForVisualization($data);
}
