<?php

namespace App\Adapters;

use App\Adapters\Interfaces\HighchartsAdapterInterface;

class HighchartsLineSeriesAdapter implements HighchartsAdapterInterface
{
    const ONBOARDING_PERC = Array(
        0 => 'Create account',
        20 => 'Activate account',
        40 => 'Provide profile information',
        50 => 'What jobs are you interested in?',
        70 => 'Do you have relevant experience in these jobs?',
        90 => 'Are you a freelancer?',
        99 => 'Waiting for approval',
        100 => 'Approval '
    );
    const TOTAL = 'total';

    public function prepareDataForVisualization($obf_data)
    {
        $grouped_data = $this->groupObfData($obf_data);
        $grouped_data = $this->convertToPercentageObfData($grouped_data);
        return $grouped_data;
    }

    public function groupObfData($obf_data)
    {
        $grouped_data = [];
        foreach ($obf_data as $data) {
            //Extracting Week from date to group on it
            $week = $data->created_at->format("W");
            $data->week = $week;

            //Initializing TOTAL of current week to one, this will only happen on the first occurrence of a data object in said week
            $grouped_data = $this->initializeWeekTotalToOne($week, $grouped_data);

            if (array_key_exists($week, $grouped_data)) {
                if ($this->isCurrentOnboardingStageInitialized($data, $grouped_data, $week)) {
                    //key exists, append to it
                    $grouped_data[$week][$data->onboarding_perentage] = $this->updateCurrentOnboardingStageByOne($grouped_data, $week, $data);
                    $grouped_data[$week][self::TOTAL] = $this->updateTotalByOne($grouped_data, $week);
                } else {
                    //insert new key
                    if ($this->isCurrentOnboardingStageValid($data)) {
                        $grouped_data = $this->initializeCurrentOnboardingStageToOne($grouped_data, $week, $data);
                    }
                    $grouped_data[$week][self::TOTAL] = $this->updateTotalByOne($grouped_data, $week);
                }
            } else {
                //insert new key
                if ($this->isCurrentOnboardingStageValid($data))
                    $grouped_data = $this->initializeCurrentOnboardingStageToOne($grouped_data, $week, $data);
            }
        }
        return $grouped_data;
    }

    public function convertToPercentageObfData($grouped_data)
    {
        $final_percentage_data = [];
        foreach ($grouped_data as $week_key => $week_val) {
            //Keeping an index of what has previously transpired
            $cumulative_perc = 0;
            $week_total = $week_val[self::TOTAL];

            $week_val = $this->popTotalValueFromList($week_val);
            $week_val = $this->appendMissingStageValues($week_val);
            krsort($week_val);
            //Sorting in descending order
            foreach ($week_val as $obf_single_stage_key => $obf_single_stage_val) {
                //Percentage of users at this stage
                $obf_single_stage_perc = $this->getCurrentOnboardingStagePercentage($obf_single_stage_val, $week_total);

                //Storing old value for later use to calculate further plot percentages
                $current_obf_single_stage_val = $obf_single_stage_perc;

                //Getting plot percentage
                $plot_percenteage = 100 - $cumulative_perc - $obf_single_stage_perc;

                //Storing plot percentage
                $week_val[$obf_single_stage_key] = $plot_percenteage;
                $cumulative_perc += $current_obf_single_stage_val;
            }
            //Storing week's data
            $final_percentage_data[$week_key] = $week_val;
        }
        return $final_percentage_data;
    }

    public function appendMissingStageValues($week_val)
    {
        foreach (self::ONBOARDING_PERC as $key => $value) {
            if (!array_key_exists($key, $week_val)) {
                $week_val[$key] = 0;
            }
        }
        return $week_val;
    }

    public function popTotalValueFromList($week_val)
    {
        krsort($week_val);
        array_pop($week_val);
        return $week_val;
    }

    public function initializeWeekTotalToOne($week, array $grouped_data): array
    {
        if (array_key_exists($week, $grouped_data) && !array_key_exists(self::TOTAL, $grouped_data[$week])) {
            $grouped_data[$week][self::TOTAL] = 1;
        }
        return $grouped_data;
    }

    public function updateTotalByOne(array $grouped_data, $week)
    {
        return $grouped_data[$week][self::TOTAL] + 1;
    }

    public function isCurrentOnboardingStageValid($data): bool
    {
        return array_key_exists($data->onboarding_perentage, self::ONBOARDING_PERC);
    }

    public function initializeCurrentOnboardingStageToOne(array $grouped_data, $week, $data): array
    {
        $grouped_data[$week][$data['onboarding_perentage']] = 1;
        return $grouped_data;
    }

    public function updateCurrentOnboardingStageByOne(array $grouped_data, $week, $data)
    {
        return $grouped_data[$week][$data['onboarding_perentage']] + 1;
    }

    public function isCurrentOnboardingStageInitialized($data, array $grouped_data, $week): bool
    {
        return array_key_exists($data['onboarding_perentage'], $grouped_data[$week]);
    }

    public function getCurrentOnboardingStagePercentage($obf_single_stage_val, $week_total)
    {
        return (($obf_single_stage_val / $week_total) * 100);
    }
}