<?php

namespace App\Repositories;

use App\Models\Builders\OnBoardingFlowBuilder;
use App\Models\OnboardingFlow;
use App\Repositories\Interfaces\OnboardingFlowRepositoryInterface;
use App\Models\Directors\OnBoardingFlowDirector;
use Exception;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Log;

class OnboardingFlowRepository implements OnboardingFlowRepositoryInterface
{
    const EXPORT_CSV = "export.csv";

    public function getAll()
    {
        $obfCSVFilePath = storage_path($this::EXPORT_CSV);
        return $this->parseCSVData($obfCSVFilePath);
    }

    public function getByDate(Date $date)
    {
        // TODO: Implement getByDate() method.
    }

    private function parseCSVData($file_path)
    {
        $parsed_data = [];
        try {
            //Checking if File Exists
            if (($handle = fopen($file_path, "r")) !== FALSE) {
                fgetcsv($handle);
                //Keep looping until EOF
                while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                    array_push($parsed_data, $this->buildOnboardingFlow($data));
                }
                fclose($handle);
            }
        } catch (Exception $exception) {
            Log::error("An Exception occured while trying to fetch data." . $exception);
        }
        return $parsed_data;
    }

    private function buildOnboardingFlow($data)
    {
        $onBoardingFlow = (new OnBoardingFlowDirector())->build(new OnBoardingFlowBuilder(new OnboardingFlow()), $data);
        return $onBoardingFlow;
    }

}