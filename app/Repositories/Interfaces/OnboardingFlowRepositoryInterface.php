<?php

namespace App\Repositories\Interfaces;

use Illuminate\Support\Facades\Date;

interface OnboardingFlowRepositoryInterface
{
    public function getAll();

    public function getByDate(Date $date);
}