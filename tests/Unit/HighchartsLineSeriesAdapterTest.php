<?php

namespace Test\Unit;

use App\Adapters\HighchartsLineSeriesAdapter;
use Tests\TestCase;

class HighchartsLineSeriesAdapterTest extends TestCase
{
    const TOTAL = 'total';
    protected $highchartsLineSeriesAdapter;
    protected $grouped_data;
    protected $week;
    protected $week2;
    protected $week3;
    protected $unint_grouped_data;
    protected $week_stages_values;
    protected $single_stage_week_data;
    protected $week_stages_values_missing_one;
    protected $grouped_data2;
    protected $obf_data;
    protected $single_raw_data;

    public function setUp(): void
    {
        parent::setUp();
        $this->week_stages_values = Array(90 => 3, self::TOTAL => 3);
        $this->unint_grouped_data = Array(29 => Array(40 => 2),
            30 => Array(40 => 3, 90 => 2),
            31 => Array(40 => 5, 70 => 3));

        $this->grouped_data = Array(29 => Array(40 => 2, self::TOTAL => 1));
        $this->week = "29";
        $this->week2 = 30;
        $this->week3 = 31;

        $this->single_stage_week_data = ["user_id" => "3122",
            "created_at" => "2016-07-19 00:00:00",
            "onboarding_perentage" => "40",
            "count_applications" => "0",
            "count_accepted_applications" => "0",
            "week" => "29"];

        $this->week_stages_values_missing_one = Array(
            20 => 2,
            40 => 3,
            50 => 2,
            70 => 3,
            90 => 2,
            99 => 3,
            100 => 4
        );

        $this->grouped_data2 = [
            30 => [
                100 => 36,
                "total" => 158,
                40 => 81,
                99 => 19,
                50 => 2,
            ],
            31 => [
                99 => 14,
                "total" => 62,
                40 => 20,
                100 => 12,
                50 => 1
            ]
        ];

        $this->single_raw_data = [
            0 => "3121",
            1 => "2016-07-19",
            2 => "40",
            3 => "0",
            4 => "0",
        ];


    }

    public function testCurrentOnboardingStagePercentage()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->getCurrentOnboardingStagePercentage(20, 40);
        $this->assertEquals($result, 50);
    }

    public function testIsCurrentOnboardingStageInitialized()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->isCurrentOnboardingStageInitialized($this->single_stage_week_data, $this->grouped_data, $this->week);
        $this->assertEquals($result, true);

    }

    public function testUpdateCurrentOnboardingStageByOne()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->updateCurrentOnboardingStageByOne($this->grouped_data, $this->week, $this->single_stage_week_data);
        $this->assertEquals($result, 3);
    }

    public function testInitializeCurrentOnBoardingStageToOne()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->initializeCurrentOnboardingStageToOne($this->grouped_data, $this->week, $this->single_stage_week_data);
        $this->assertArrayHasKey(self::TOTAL, $result[$this->week]);
        $this->assertEquals($result[$this->week][self::TOTAL], 1);
    }

    public function testUpdateTotalByOneToTrue()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->updateTotalByOne($this->grouped_data, $this->week);
        $this->assertEquals($result, 2);
    }

    public function testInitializeWeekTotalToOne()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->initializeWeekTotalToOne($this->week, $this->unint_grouped_data);
        $this->assertEquals($result[$this->week][self::TOTAL], 1);
    }

    public function testInitializeWeekTotalToOneWhenAlreadyInitialized()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->initializeWeekTotalToOne($this->week, $this->grouped_data);
        $this->assertEquals($result[$this->week][self::TOTAL], 1);
    }

    public function testPopTotalValueFromList()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->popTotalValueFromList($this->week_stages_values);
        $this->assertArrayNotHasKey(self::TOTAL, $result);
    }

    public function testAppendMissingStageValues()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();
        $result = $highchartsLineSeriesAdapter->appendMissingStageValues($this->week_stages_values_missing_one);
        $this->assertArrayHasKey(0, $result);
    }

    public function testConvertToPercentageObfData()
    {
        $highchartsLineSeriesAdapter = new HighchartsLineSeriesAdapter();

        $result = $highchartsLineSeriesAdapter->convertToPercentageObfData($this->grouped_data2);
        $this->assertEquals((int)$result[$this->week2][100], 77.0);
        $this->assertEquals((int)$result[$this->week2][70], 65);
        $this->assertEquals((int)$result[$this->week3][40], 24);
    }
}
